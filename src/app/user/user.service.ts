import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable, of } from 'rxjs';


export interface User {
  name: string;
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(environment.apiUrl + 'user', user);
  }

  getUser(userId: string) {
    return this.http.get<User>(environment.apiUrl + 'user/' + userId);
  }

  getAllUser() {
    return this.http.get<User[]>(environment.apiUrl + 'user/');
  }

}
