import { Component, OnInit } from '@angular/core';
import { UserService, User } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers: [UserService],
})
export class UserListComponent implements OnInit {

  userList: User[];

  constructor(private userService: UserService, ) { }

  ngOnInit(): void {
    this.userService.getAllUser().subscribe(
      userList => this.userList = userList);
  };

}
