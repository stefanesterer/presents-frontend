import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css'],
  providers: [UserService],
})
export class UserAddComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl(''),
    emailAddress: new FormControl(''),
  });

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.userService.createUser(this.form.value).subscribe(
      user => this.router.navigate(['/user', user.id]));
  }

}
