import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';

export interface Present {
  name: string;
  id: string;
}

@Injectable()
export class PresentService {


  constructor(private http: HttpClient) { }

  getPresents(userId: string) {
    return this.http.get<Present[]>(environment.apiUrl + 'present/' + userId);
  }

  createPresent(userId: string, present: Present) {
    this.http.post<Present>(environment.apiUrl + 'present/' + userId, present).subscribe();
  }

  deletePresent(userId: string, presentId: String): Observable<any> {
    return this.http.delete(environment.apiUrl + 'present/' + userId + "/" + presentId);
  }

}
