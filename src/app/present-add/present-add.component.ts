import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { PresentService } from '../present/present.service';

@Component({
  selector: 'app-present-add',
  templateUrl: './present-add.component.html',
  styleUrls: ['./present-add.component.css'],
  providers: [PresentService],
})
export class PresentAddComponent implements OnInit {

  presentForm = new FormGroup({
    name: new FormControl('')
  });

  @Input() userId: string;

  constructor(private presentService: PresentService) { }

  ngOnInit(): void {
  }

  updateName() {
    this.presentForm.setValue({ name: 'Nancy' });
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.presentService.createPresent(this.userId, this.presentForm.value);
  }

}
