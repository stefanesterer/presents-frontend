import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentAddComponent } from './present-add.component';

describe('PresentAddComponent', () => {
  let component: PresentAddComponent;
  let fixture: ComponentFixture<PresentAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresentAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
