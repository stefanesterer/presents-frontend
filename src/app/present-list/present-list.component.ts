import { Component, OnInit } from '@angular/core';
import { PresentService, Present } from '../present/present.service';
import { ActivatedRoute } from '@angular/router';
import { UserService, User } from '../user/user.service';

@Component({
  selector: 'app-present-list',
  templateUrl: './present-list.component.html',
  styleUrls: ['./present-list.component.css'],
  providers: [PresentService, UserService],
})


export class PresentListComponent implements OnInit {

  presents: Present[];
  user: User;

  constructor(private presentService: PresentService, private userService: UserService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      let userId = params.get('id');
      this.userService.getUser(userId)
        .subscribe(user => this.user = user);
    });
  }

  showPresents() {
    this.presentService.getPresents(this.user.id)
      .subscribe((data: Present[]) => this.presents = data);
  }

  delete(userId: string, presentId: String) {
    this.presentService.deletePresent(userId, presentId).subscribe(
      data => {
        // this.reloadData();
      },
      error => console.log(error));
  }


}
